# Binutils Avr


## GNU binary utilities for avr

This package has been compiled to target the  avr architecture.
The GNU Binutils are a collection of binary tools. The main ones are:
ld - the GNU linker.
as - the GNU assembler.
gold - a new, faster, ELF only linker.
And many others.

This package is primarily for avr developers and cross-compilers and
is not needed by normal users or developers.
