set -e
V=$(head -1 debian/changelog  | sed -e 's/.*(//' -e 's/-.*//')
echo $V
tar czf ../binutils-avr_$V.orig.tar.gz release.sh README.md
# debuild --changes-option=-S --source-only-changes
#dpkg-buildpackage --changes-option=-S
debuild -S
debrelease -S
